
import sqlite3
from sqlite3 import Error
import os
from os import path
from devops import db, app, DB_PATH
from devops.models import Usuario,TipoUsuario,Setor
from devops.utils.crypt import generate_hash

def create_db(db_file):
    """ create a database connection to a SQLite database """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        print(db_file)
    except Error as e:
        print(e)
    finally:
        if conn:
            conn.close()

def create_tables():
	db.create_all()
	db.session.commit()
 
def add_admin_user(usuario,email,senha,nome):
	setor_default = Setor(nome='Default',numero_sala='1')
	# db.session.add(setor_default)
	# db.session.commit()

	# tipo_usuario_admin = TipoUsuario(nome='Administrador',admin=1)
	# db.session.add(tipo_usuario_admin)
	# db.session.commit()

	usuario = Usuario(login=usuario,email=email,senha=generate_hash(senha,metodo=app.config['METODO_ENCRIPTACAO']),nome=nome,
					  ativo=1,id_tipo_usuario=1,id_setor=1)	

	db.session.add(usuario)
	db.session.commit()


if __name__ == '__main__':
	db_path = os.getenv('DB_PATH')
	if db_path:
		if path.exists(db_path):
			create_db(db_path)
			create_tables()
			add_admin_user('admin','admin@admin.com','admin','Administrador')
	else:    	
		print('You must define the DB_PATH environment variable pointing to sqlite3 database. So, try again.')
