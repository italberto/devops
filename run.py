from devops import app
from waitress import serve
import sys
import socket


if __name__ == '__main__':
    help_msg = '''USO: ./run.py [-p|-d|(-h|--help)|]'''
    args = sys.argv
    if len(args)>1:
        ip = socket.gethostbyname(socket.gethostname())
        opcao = args[1]
        if opcao == '-p': #Producao
            serve(app,host=ip,port=80)
        elif opcao == '-d': #Desenvolvimento
            app.run(debug=True)
        elif opcao == '-h' or opcao == '--help':
            print(help_msg)
    else:
        print(help_msg)
    app.run(debug=True)