# -*- coding: utf-8 -*-
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField,IntegerField,SelectField,PasswordField,BooleanField
from wtforms.validators import DataRequired, Length, Email
from wtforms.validators import ValidationError
from devops.models import Usuario
from flask_login import current_user

class FormularioCadastroUsuario(FlaskForm):
    id = IntegerField('ID')
    login_field = StringField('Usuário', 
                            validators=[DataRequired(),Length(min=5,max=20)]
                          )
    email = StringField('Email',validators=[DataRequired(),Email()])
    nome = StringField('Nome',validators=[DataRequired()])
    senha_field = PasswordField('Senha',validators=[DataRequired()])
    tipo_usuario = SelectField('Tipo de Usuário',coerce=int)
    setor = SelectField('Setor',coerce=int)
    ativo = BooleanField('Ativo')
    submit = SubmitField('Salvar')

    def validate_login(self,login):
        if login.data != current_user.login:
            usuario = Usuario.query.filter_by(login=login.data).first()
            if usuario:
                raise ValidationError('O login já está em uso.')

    def validate_email(self,email):
        usuario = Usuario.query.filter_by(email=email.data).first()
        if current_user.is_admin():
            if usuario.login != self.login_field.data:
                raise ValidationError('O email já está em uso.')
        else:
            if usuario and self.login_field.data != current_user.login:
                raise ValidationError('O email já está em uso.')
