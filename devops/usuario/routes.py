# -*- coding: utf-8 -*-
from flask import render_template, request, Blueprint, flash, redirect,url_for
from devops.usuario.forms import FormularioCadastroUsuario
from devops import db,app
from flask_login import login_required
from devops.models import Usuario, TipoUsuario,Setor
from devops.utils.check_roles import require_admin
from devops.utils.crypt import generate_hash
from devops.utils.visual import calcular_paginas

usuarios = Blueprint('usuarios',__name__)

@usuarios.route('/usuarios')
@login_required
@require_admin()
def usuario_list():
    page = request.args.get('page',1,type=int)
    per_page=app.config['PER_PAGE']
    
    usuarios = Usuario.query.paginate(page=page,per_page=per_page)
    return render_template('usuario/list.html',
                            title='Usuários',
                            usuarios=usuarios,
                            paginacao=calcular_paginas(usuarios,page=page))

@usuarios.route('/usuario/novo',methods=['GET','POST'])
@login_required
@require_admin()
def usuario_novo():
    form = FormularioCadastroUsuario()
    form.tipo_usuario.choices = [(t.id, t.nome) for t in TipoUsuario.query.all()]
    form.setor.choices = [(t.id, t.nome) for t in Setor.query.all()]
    if form.validate_on_submit():
        hashed_password = generate_hash(form.senha_field.data,metodo=app.config['METODO_ENCRIPTACAO'])
        
        usuario = Usuario(login=form.login_field.data,
                                  email=form.email.data,
                                  senha=hashed_password,
                                  ativo=1 if form.ativo.data == True else 0,
                                  id_tipo_usuario=form.tipo_usuario.data,
                                  id_setor=form.setor.data)

        db.session.add(usuario)
        db.session.commit()
        flash('Usuário cadastrado.','success')
        return redirect(url_for('usuarios.usuario_list'))
    elif request.method == 'GET':
        return render_template('usuario/new.html',titulo="Novo Usuário",form=form)
    else:
        return render_template('usuario/new.html',titulo="Novo Usuário",form=form)

@usuarios.route('/usuario/<int:id_usuario>')
@login_required
@require_admin()
def usuario(id_usuario):
    form = FormularioCadastroUsuario()
    usuario = Usuario.query.get_or_404(id_usuario)
    return render_template('usuario/show.html',usuario=usuario,form=form,title='Usuário')

@usuarios.route('/usuario/<int:id_usuario>/atualizar',methods=['GET'])
@login_required
@require_admin()
def editar_usuario(id_usuario):
    usuario = Usuario.query.get_or_404(id_usuario)
    
    form = FormularioCadastroUsuario()
    form.senha_field.data = app.config['FOO_PASSWD']
    form.login_field.data = usuario.login
    form.email.data = usuario.email
    form.ativo.data = True if usuario.ativo == 1 else False
    form.tipo_usuario.data = usuario.id_tipo_usuario
    form.setor.data = usuario.id_setor

    tipos_usuario = TipoUsuario.query.all()
    form.tipo_usuario.choices = [(t.id, t.nome) for t in tipos_usuario]

    return render_template('usuario/edit.html',
                            titulo='Atualizar Usuário',
                            usuario=usuario,
                            form=form,
                            id_usuario=id_usuario)

@usuarios.route('/usuario/<int:id_usuario>/atualizar',methods=['POST'])
@login_required
@require_admin()
def atualizar_usuario(id_usuario):
    usuario = Usuario.query.get_or_404(id_usuario)
    form = FormularioCadastroUsuario()
    tipos_usuario = TipoUsuario.query.all()
    form.tipo_usuario.choices = [(t.id, t.nome) for t in tipos_usuario]
    form.setor.choices = [(t.id, t.nome) for t in Setor.query.all()]
    if form.validate_on_submit():
        if form.senha_field.data != app.config['FOO_PASSWD']:
            hashed_password = generate_hash(form.senha_field.data,metodo=app.config['METODO_ENCRIPTACAO'])
            usuario.senha = hashed_password

        usuario.login = form.login_field.data
        usuario.email = form.email.data
        usuario.ativo = 1 if form.ativo.data == True else 0
        usuario.id_tipo_usuario = form.tipo_usuario.data
        usuario.id_setor = form.setor.data
        
        db.session.commit()
        flash('Usuário atualizado!','success')
        return redirect(url_for('usuarios.usuario_list'))
    else:
        return render_template('usuario/edit.html',
                            titulo='Atualizar Usuário',
                            usuario=usuario,
                            form=form,
                            id_usuario=id_usuario)


@usuarios.route('/usuario/<int:id_usuario>/remover',methods=['GET','POST'])
@login_required
@require_admin()
def remover_usuario(id_usuario):
    usuario = Usuario.query.get_or_404(id_usuario)
    if usuario != current_user:
        db.session.delete(usuario)
        db.session.commit()
        flash('Usuário excluído.','success')
        return redirect(url_for('usuarios.usuario_list'))
    else:
        flash('Você não pode excluir a sí mesmo.')
        return(render_template('usuario/usuario_list'))
    

