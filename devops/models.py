from devops import db
from devops import login_manager
from flask_login import UserMixin
from datetime import datetime

@login_manager.user_loader
def load_usuario(id_usuario):
    return Usuario.query.get(int(id_usuario))

setor_seq = db.Sequence('setor_id_seq',start=10)
class Setor(db.Model):
    __tablename__ = 'setor'
    id = db.Column(db.Integer, setor_seq, primary_key=True,autoincrement=True, server_default=setor_seq.next_value())
    nome = db.Column(db.String,nullable=False)
    numero_sala = db.Column(db.String)

banco_trabalho_seq = db.Sequence('banco_trabalho_id_seq',start=10)
class BancoTrabalho(db.Model):
    __tablename__ = 'banco_trabalho'
    id = db.Column(db.Integer,banco_trabalho_seq,primary_key=True,autoincrement=True,server_default=banco_trabalho_seq.next_value())
    nome = db.Column(db.String) 
    porta = db.Column(db.Integer)
    nome_database = db.Column(db.String)
    host_name = db.Column(db.String)
    usuario_database = db.Column(db.String)
    senha_database = db.Column(db.String) 
    id_ambiente_banco = db.Column(db.Integer,db.ForeignKey('ambiente.id'),nullable=False) 
    ambiente_banco = db.relationship('AmbienteBanco',backref='bancos_trabalho',lazy=True)
    id_tipo_sgbd = db.Column(db.Integer,db.ForeignKey('tipo_sgbd.id'),nullable=False)
    tipo_sgbd = db.relationship('TipoSGBD',backref='bancos_trabalho',lazy=True)

    def url(self):
        url_padrao = self.tipo_sgbd.formato_url
        url_padrao = url_padrao.replace('<host>',str(self.host_name))
        url_padrao = url_padrao.replace('<port>',str(self.porta))
        url_padrao = url_padrao.replace('<database>',str(self.nome_database))

        return url_padrao

    def url_with_user(self):
        url_padrao = self.tipo_sgbd.formato_url
        user_host = '{}:{}@{}'.format(self.usuario_database,self.senha_database,str(self.host_name))
        url_padrao = url_padrao.replace('<host>',user_host)
        url_padrao = url_padrao.replace('<port>',str(self.porta))
        url_padrao = url_padrao.replace('<database>',str(self.nome_database))

        return url_padrao

usuario_seq = db.Sequence('usuario_id_seq',start=10)
class Usuario(db.Model, UserMixin):
    __tablename__ = 'usuario'
    id = db.Column(db.Integer, usuario_seq, primary_key=True,autoincrement=True, server_default=usuario_seq.next_value())
    login = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(150), unique=True, nullable=False)
    senha = db.Column(db.String(60), nullable=False)
    nome = db.Column(db.String) 
    ativo = db.Column(db.Integer,nullable=False)
    id_tipo_usuario = db.Column(db.Integer,db.ForeignKey('tipo_usuario.id'),nullable=False) 
    tipo_usuario = db.relationship('TipoUsuario',backref='usuarios',lazy=True)
    id_setor = db.Column(db.Integer,db.ForeignKey('setor.id'),nullable=False)
    setor = db.relationship('Setor',backref='usuarios',lazy=True)
    

    def is_admin(self):
        return True if self.tipo_usuario.admin == 1 else False

    def is_ativo(self):
        return True if self.ativo == 1 else False

tipo_usuario_seq = db.Sequence('tipo_usuario_id_seq',start=10)
class TipoUsuario(db.Model):
    __tablename__ = 'tipo_usuario'
    id = db.Column(db.Integer, tipo_usuario_seq, primary_key=True, nullable=False,autoincrement=True, server_default=tipo_usuario_seq.next_value())
    nome = db.Column(db.String(20), unique=True, nullable=False)
    admin = db.Column(db.Integer,nullable=False)
    
ambiente_banco_seq = db.Sequence('ambiente_banco_id_seq',start=10)    
class AmbienteBanco(db.Model):
    __tablename__ = 'ambiente'
    id = db.Column(db.Integer, ambiente_banco_seq, primary_key=True, nullable=False,autoincrement=True, server_default=ambiente_banco_seq.next_value())
    descricao = db.Column(db.String(20), unique=True, nullable=False)

grupo_banco_seq = db.Sequence('grupo_banco_id_seq',start=10)    
class GrupoBanco(db.Model):
    __tablename__ = 'grupo_banco'
    id = db.Column(db.Integer, grupo_banco_seq, primary_key=True, nullable=False,autoincrement=True, server_default=grupo_banco_seq.next_value())
    descricao = db.Column(db.String(20), unique=True, nullable=False)
    
tipo_sgbd_seq = db.Sequence('tipo_sgbd_id_seq',start=10)        
class TipoSGBD(db.Model):
    __tablename__ = 'tipo_sgbd'
    id = db.Column(db.Integer, tipo_sgbd_seq, primary_key=True, nullable=False,autoincrement=True, server_default=tipo_sgbd_seq.next_value())
    descricao = db.Column(db.String(20), unique=True, nullable=False)
    formato_url = db.Column(db.String(150),nullable=False)
    porta_padrao = db.Column(db.Integer,nullable=False)
    
tipo_solicitacao_seq = db.Sequence('tipo_solicitacao_id_seq',start=10)        
class TipoSolicitacao(db.Model):
    __tablename__ = 'tipo_solicitacao'
    id = db.Column(db.Integer, tipo_solicitacao_seq, primary_key=True, nullable=False,autoincrement=True, server_default=tipo_solicitacao_seq.next_value())
    descricao = db.Column(db.String(20), unique=True, nullable=False)
    
linguagem_comentario_seq = db.Sequence('linguagem_comentario_id_seq',start=10)        
class LinguagemSuportada(db.Model):
    __tablename__ = 'linguagem_comentario'
    id = db.Column(db.Integer, linguagem_comentario_seq, primary_key=True, nullable=False,autoincrement=True, server_default=linguagem_comentario_seq.next_value())
    descricao = db.Column(db.String(20), unique=True, nullable=False)

situacao_solicitacao_seq = db.Sequence('situacao_solicitacao_id_seq',start=10)    
class SituacaoSolicitacao(db.Model):
    __tablename__ = 'situacao_solicitacao'

    id = db.Column(db.Integer, situacao_solicitacao_seq, primary_key=True,autoincrement=True, server_default=situacao_solicitacao_seq.next_value())
    descricao = db.Column(db.String(20), unique=True, nullable=False)
    ativa = db.Column(db.Integer,nullable=False) 
    padrao_nova = db.Column(db.Integer,nullable=False)
    executada_sucesso = db.Column(db.Integer)

    def is_cancelada(self):
        return True if self.executada_sucesso == 0 and self.ativa == 0 else False

    def is_padrao_nova(self):
        return True if self.padrao_nova == 1 else False

    def is_ativo(self):
        if self.ativa == 1:
            return True
        else:
            return False

solicitacao_seq = db.Sequence('solicitacao_id_seq',start=10)    
class Solicitacao(db.Model):
    __tablename__ = 'solicitacao'
    id = db.Column(db.Integer, solicitacao_seq, primary_key=True, nullable=False,autoincrement=True, server_default=solicitacao_seq.next_value())
    titulo = db.Column(db.String, nullable=False)
    texto = db.Column(db.String)
    script = db.Column(db.String)
    
    data_solicitacao = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    data_finalizacao = db.Column(db.DateTime)
    
    id_tipo_solicitacao = db.Column(db.Integer,db.ForeignKey('tipo_solicitacao.id'),nullable=False)
    tipo_solicitacao = db.relationship('TipoSolicitacao',backref='usuarios',lazy=True)

    checada = db.Column(db.Integer,nullable=False,default=0)

    id_ambiente = db.Column(db.Integer,db.ForeignKey('ambiente.id'),nullable=False) 
    ambiente = db.relationship('AmbienteBanco',backref='usuarios',lazy=True)

    id_banco = db.Column(db.Integer,db.ForeignKey('banco_trabalho.id'),nullable=False) 
    banco = db.relationship('BancoTrabalho',backref='usuarios',lazy=True)

    id_situacao = db.Column(db.Integer,db.ForeignKey('situacao_solicitacao.id'),nullable=False)
    situacao = db.relationship('SituacaoSolicitacao',backref='usuarios',lazy=True)

    id_solicitante = db.Column(db.Integer,db.ForeignKey('usuario.id'),nullable=False) 
    solicitante = db.relationship('Usuario',backref='solicitante',lazy=True,foreign_keys=[id_solicitante])

    id_analista_responsavel = db.Column(db.Integer,db.ForeignKey('usuario.id')) 
    responsavel = db.relationship('Usuario',backref='responsavel',lazy=True,foreign_keys=[id_analista_responsavel])

    def get_data_solicitacao(self):
        return self.data_solicitacao.strftime("%d/%m/%Y %H:%M")

    def get_data_finalizacao(self):
        return self.data_finalizacao.strftime("%d/%m/%Y %H:%M")

    def titulo_curto(self,padrao=120):
        if len(self.titulo)>padrao:
            return '{}...'.format(self.titulo[0:padrao])
        else:
            return self.titulo

    def is_checked(self):
        return True if self.checada == 1 else False

    def sem_dono(self):
        return True if not self.responsavel else False