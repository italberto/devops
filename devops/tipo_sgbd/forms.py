# -*- coding: utf-8 -*-
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField,HiddenField, IntegerField
from wtforms.validators import DataRequired,ValidationError
from devops.models import TipoSGBD


class FormularioTipoSGBD(FlaskForm):
    id = IntegerField('ID')
    descricao = StringField('Descrição', validators=[DataRequired()])                   
    formato_url = StringField('Formato URL', validators=[DataRequired()])                   
    porta_padrao = IntegerField('Porta Padrão')
    submit = SubmitField('Salvar')  

    def validate_porta_padrao(self,porta_padrao):
        if not str(porta_padrao.data).isnumeric():
            raise ValidationError('Apenas números são permitidos.')
