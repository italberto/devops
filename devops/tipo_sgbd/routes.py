# -*- coding: utf-8 -*-
from flask import render_template, request, Blueprint, flash, redirect,url_for
from devops.models import TipoSGBD
from devops.tipo_sgbd.forms import FormularioTipoSGBD
from devops import db
from flask_login import login_required
from devops.utils.check_roles import require_admin

tipos_sgbd = Blueprint('tipos_sgbd',__name__)


@tipos_sgbd.route('/tipos_sgbd')
@login_required
@require_admin()
def tipo_sgbd_list():
    page = request.args.get('page',1,type=int)
    total = len(TipoSGBD.query.all())
    per_page = 10
    next = None
    previous = None
    if page > 1:
        previous = page - 1
    if page < total/per_page:
        next = page + 1 
    tipos_sgbd = TipoSGBD.query.paginate(page=page,per_page=per_page)
    return render_template('tipo_sgbd/list.html',
                            title='Tipos de SGBD',
                            tipos_sgbd=tipos_sgbd,
                            next=next,
                            previous=previous,
                            total=total)

@tipos_sgbd.route('/tipo_sgbd/novo',methods=['GET','POST'])
@login_required
@require_admin()
def tipo_sgbd_novo():
    form = FormularioTipoSGBD()
    if form.validate_on_submit():
        tipo_sgbd = TipoSGBD(descricao=form.descricao.data,formato_url=form.formato_url.data,porta_padrao=form.porta_padrao.data)
        db.session.add(tipo_sgbd)
        db.session.commit()
        flash('Tipo de SGBD cadastrada.','success')
        return redirect(url_for('tipos_sgbd.tipo_sgbd_list'))
    elif request.method == 'GET':
        return render_template('tipo_sgbd/new.html',titulo="Novo Tipo de SGBD",form=form)
    else:
        return render_template('tipo_sgbd/new.html',titulo="Novo Tipo de SGBD",form=form)

@tipos_sgbd.route('/tipo_sgbd/<int:id_tipo_sgbd>')
@login_required
@require_admin()
def tipo_sgbd(id_tipo_sgbd):
    tipo_sgbd = TipoSGBD.query.get_or_404(id_tipo_sgbd)
    form = FormularioTipoSGBD()
    form.id.data = tipo_sgbd.id
    form.descricao.data = tipo_sgbd.descricao
    form.formato_url.data = tipo_sgbd.formato_url
    form.porta_padrao.data = tipo_sgbd.porta_padrao
    return render_template('tipo_sgbd/show.html',tipo_sgbd=tipo_sgbd,form=form,title='Tipo de SGBD')

@tipos_sgbd.route('/tipo_sgbd/<int:id_tipo_sgbd>/atualizar',methods=['GET'])
@login_required
@require_admin()
def editar_tipo_sgbd(id_tipo_sgbd):
    tipo_sgbd = TipoSGBD.query.get_or_404(id_tipo_sgbd)
    form = FormularioTipoSGBD()
    form.descricao.data = tipo_sgbd.descricao
    form.formato_url.data = tipo_sgbd.formato_url
    form.porta_padrao.data = tipo_sgbd.porta_padrao
    return render_template('tipo_sgbd/edit.html',
                            titulo='Atualizar Tipo de SGBD',
                            tipo_sgbd=tipo_sgbd,
                            form=form,
                            id_tipo_sgbd=id_tipo_sgbd)

@tipos_sgbd.route('/tipo_sgbd/<int:id_tipo_sgbd>/atualizar',methods=['POST'])
@login_required
@require_admin()
def atualizar_tipo_sgbd(id_tipo_sgbd):
    tipo_sgbd = TipoSGBD.query.get_or_404(id_tipo_sgbd)
    form = FormularioTipoSGBD()
    if form.validate_on_submit():
        tipo_sgbd.descricao = form.descricao.data
        tipo_sgbd.formato_url = form.formato_url.data
        tipo_sgbd.porta_padrao = form.porta_padrao.data 
        db.session.commit()
        flash('Registro atualizado!','success')
        return redirect(url_for('tipos_sgbd.tipo_sgbd_list'))
    else:
        return render_template('tipo_sgbd/edit.html',
                            titulo='Atualizar Tipo de SGBD',
                            tipo_sgbd=tipo_sgbd,
                            form=form,
                            id_tipo_sgbd=id_tipo_sgbd)


@tipos_sgbd.route('/tipo_sgbd/<int:id_tipo_sgbd>/remover',methods=['GET','POST'])
@login_required
@require_admin()
def remover_tipo_sgbd(id_tipo_sgbd):
    print(id_tipo_sgbd)
    
    tipo_sgbd = TipoSGBD.query.get_or_404(id_tipo_sgbd)
    print(tipo_sgbd)
    db.session.delete(tipo_sgbd)
    db.session.commit()
    flash('O registro foi excluído.','success')
    return redirect(url_for('tipos_sgbd.tipo_sgbd_list'))
    
