# -*- coding: utf-8 -*-
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, HiddenField, IntegerField, BooleanField
from wtforms.validators import DataRequired, Length,ValidationError
from devops.models import SituacaoSolicitacao

class FormularioSituacaoSolicitacao(FlaskForm):
    id = IntegerField('ID')
    descricao = StringField('Descrição',validators=[DataRequired(),Length(min=4,max=20)])
    ativo = BooleanField('Ativo')
    padrao_nova = BooleanField('Padrão de Nova Tarefa')
    submit = SubmitField('Salvar')

    def validate_padrao_nova(self,padrao_nova):
        situacao = SituacaoSolicitacao.query.filter_by(padrao_nova=1).first()
        if situacao:
            if padrao_nova.data == 1:
                raise ValidationError('Já existe uma situação padrão cadastrada.')
