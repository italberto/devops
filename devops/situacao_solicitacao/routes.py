# -*- coding: utf-8 -*-
from flask import render_template, request, Blueprint, flash, redirect,url_for
from devops.models import SituacaoSolicitacao
from devops.situacao_solicitacao.forms import FormularioSituacaoSolicitacao
from devops import db
from flask_login import login_required
from devops.utils.check_roles import require_admin

situacoes_solicitacao = Blueprint('situacoes_solicitacao',__name__)


@situacoes_solicitacao.route('/situacoes_solicitacao')
@login_required
@require_admin()
def situacao_solicitacao_list():
    page = request.args.get('page',1,type=int)
    total = len(SituacaoSolicitacao.query.all())
    per_page = 10
    next = None
    previous = None
    if page > 1:
        previous = page - 1
    if page < total/per_page:
        next = page + 1 
    situacoes_solicitacao = SituacaoSolicitacao.query.paginate(page=page,per_page=per_page)
    return render_template('situacao_solicitacao/list.html',
                            title='Situação Solicitação',
                            situacoes_solicitacao=situacoes_solicitacao,
                            next=next,
                            previous=previous,
                            total=total)

@situacoes_solicitacao.route('/situacao_solicitacao/novo',methods=['GET','POST'])
@login_required
@require_admin()
def situacao_solicitacao_novo():
    form = FormularioSituacaoSolicitacao()
    if form.validate_on_submit():
        situacao_solicitacao = SituacaoSolicitacao(
                descricao=form.descricao.data,
                ativa=form.ativo.data,
                padrao_nova=form.padrao_nova.data)
        db.session.add(situacao_solicitacao)
        db.session.commit()
        flash('Situação Cadastrada.','success')
        return redirect(url_for('situacoes_solicitacao.situacao_solicitacao_list'))
    elif request.method == 'GET':
        return render_template('situacao_solicitacao/new.html',titulo="Nova Situação",form=form)
    else:
        return render_template('situacao_solicitacao/new.html',titulo="Nova Situação",form=form)

@situacoes_solicitacao.route('/situacao_solicitacao/<int:id_situacao_solicitacao>')
@login_required
@require_admin()
def situacao_solicitacao(id_situacao_solicitacao):
    situacao_solicitacao = SituacaoSolicitacao.query.get_or_404(id_situacao_solicitacao)
    form = FormularioSituacaoSolicitacao()
    return render_template('situacao_solicitacao/show.html',situacao_solicitacao=situacao_solicitacao,form=form,title='situacao_solicitacao')   

@situacoes_solicitacao.route('/situacao_solicitacao/<int:id_situacao_solicitacao>/atualizar',methods=['GET'])
@login_required
@require_admin()
def editar_situacao_solicitacao(id_situacao_solicitacao):
    situacao_solicitacao = SituacaoSolicitacao.query.get_or_404(id_situacao_solicitacao)
    form = FormularioSituacaoSolicitacao()
    form.descricao.data = situacao_solicitacao.descricao
    form.ativo.data = situacao_solicitacao.ativa
    form.padrao_nova.data = situacao_solicitacao.padrao_nova
    return render_template('situacao_solicitacao/edit.html',
                            titulo='Atualizar Situação Solicitação',
                            situacao_solicitacao=situacao_solicitacao,
                            form=form,
                            id_situacao_solicitacao=id_situacao_solicitacao)

@situacoes_solicitacao.route('/situacao_solicitacao/<int:id_situacao_solicitacao>/atualizar',methods=['POST'])
@login_required
@require_admin()
def atualizar_situacao_solicitacao(id_situacao_solicitacao):
    situacao_solicitacao = SituacaoSolicitacao.query.get_or_404(id_situacao_solicitacao)
    form = FormularioSituacaoSolicitacao()
    if form.validate_on_submit():
        situacao_solicitacao.descricao = form.descricao.data
        situacao_solicitacao.ativa = form.ativo.data
        situacao_solicitacao.padrao_nova = form.padrao_nova.data
        db.session.commit()
        flash('Registro atualizado!','success')
        return redirect(url_for('situacoes_solicitacao.situacao_solicitacao_list'))
    else:
        return render_template('situacao_solicitacao/edit.html',
                            titulo='Atualizar Situação Solicitação',
                            situacao_solicitacao=situacao_solicitacao,
                            form=form,
                            id_situacao_solicitacao=id_situacao_solicitacao)


@situacoes_solicitacao.route('/situacao_solicitacao/<int:id_situacao_solicitacao>/remover',methods=['GET','POST'])
@login_required
@require_admin()
def remover_situacao_solicitacao(id_situacao_solicitacao):
    print(id_situacao_solicitacao)
    
    situacao_solicitacao = SituacaoSolicitacao.query.get_or_404(id_situacao_solicitacao)
    print(situacao_solicitacao)
    db.session.delete(situacao_solicitacao)
    db.session.commit()
    flash('O registro foi excluído.','success')
    return redirect(url_for('situacoes_solicitacao.situacao_solicitacao_list'))
