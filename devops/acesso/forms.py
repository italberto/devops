# -*- coding: utf-8 -*-
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField,PasswordField,BooleanField
from wtforms.validators import DataRequired, Length,Email,EqualTo,ValidationError
from devops.models import Usuario


class FormularioRegistro(FlaskForm):
    login = StringField('Usuário', 
                            validators=[DataRequired(),Length(min=5,max=20)]
                          )
    nome = StringField('Nome')
    email = StringField('Email',validators=[DataRequired(),Email()])
    senha = PasswordField('Senha',validators=[DataRequired()])
    confirmacao_senha = PasswordField('Confirmação de Senha',validators=[DataRequired(),EqualTo('senha')])
    submit = SubmitField('Registar')

    def validate_login(self,login):
        usuario = Usuario.query.filter_by(login=login.data).first()
        if usuario:
            raise ValidationError('O login já está em uso.')

    def validate_email(self,email):
        usuario = Usuario.query.filter_by(email=email.data).first()
        if usuario:
            raise ValidationError('O email já está em uso.')

class FormularioLogin(FlaskForm):
    login = StringField('Usuário', 
                            validators=[DataRequired(),Length(min=5,max=20)]
                          )
    senha = PasswordField('Senha',validators=[DataRequired()])
    lembrar_login = BooleanField('Lembrar de mim')
    submit = SubmitField('Entrar')
