# -*- coding: utf-8 -*-
from flask import render_template, request, Blueprint, flash, redirect,url_for
from devops.models import TipoSolicitacao,Usuario
from devops.acesso.forms import FormularioRegistro,FormularioLogin
from devops import db
from flask_login import login_required,logout_user,current_user,login_user
from devops.utils.crypt import generate_hash,check_password
from devops import app

acesso = Blueprint('acesso',__name__)

@acesso.route('/cadastrar',methods=['GET','POST'])
def cadastrar():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
       
    form = FormularioRegistro()
    if form.validate_on_submit():
        
        hashed_password = generate_hash(form.senha.data,metodo=app.config['METODO_ENCRIPTACAO'])
        
        usuario = Usuario(login=form.login.data,
                          senha=hashed_password,
                          email=form.email.data,
                          ativo=0,
                          nome=form.nome.data,
                          id_setor=1,
                          id_tipo_usuario=2) #comum
        db.session.add(usuario)
        db.session.commit()
        flash('Sua conta foi criada. Aproveite!','success')
        return redirect(url_for('acesso.login'))
    return render_template('acesso/cadastro.html',titulo='Registro',form=form)


@acesso.route('/login',methods=['GET','POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    
    form = FormularioLogin()
    if form.validate_on_submit():
        usuario = Usuario.query.filter_by(login=form.login.data).first()

        #if usuario and bcrypt.check_password_hash(usuario.senha, form.senha.data):
        
        if usuario and check_password(usuario.senha,form.senha.data):
        
            login_user(usuario,remember=form.lembrar_login.data)
            next_page = request.args.get('next')
            if next_page:
                return redirect(url_for(next_page))
            else: 
                return redirect(url_for('index'))
        else:
            flash('O login falhou!','unsucccess')
    return render_template('acesso/login.html',titulo='Login',form=form)
 
@acesso.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))
