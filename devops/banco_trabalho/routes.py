# -*- coding: utf-8 -*-
from flask import render_template, request, Blueprint, flash, redirect,url_for
from devops.models import BancoTrabalho,TipoSGBD,AmbienteBanco
from devops.banco_trabalho.forms import FormularioBancoTrabalho
from devops import db,app
from flask_login import login_required
from devops.utils.check_roles import require_admin

bancos_trabalho = Blueprint('bancos_trabalho',__name__)


@bancos_trabalho.route('/bancos_trabalho')
@login_required
@require_admin()
def banco_trabalho_list():
    page = request.args.get('page',1,type=int)
    total = len(BancoTrabalho.query.all())
    per_page = 10
    next = None
    previous = None
    if page > 1:
        previous = page - 1
    if page < total/per_page:
        next = page + 1 
    bancos_trabalho = BancoTrabalho.query.paginate(page=page,per_page=per_page)
    return render_template('banco_trabalho/list.html',
                            title='Banco de Trabalho',
                            bancos_trabalho=bancos_trabalho,
                            next=next,
                            previous=previous,
                            total=total)

@bancos_trabalho.route('/banco_trabalho/novo',methods=['GET','POST'])
@login_required
@require_admin()
def banco_trabalho_novo():
    form = FormularioBancoTrabalho()
    form.tipo_sgbd.choices = [(ts.id,ts.descricao) for ts in TipoSGBD.query.all()]
    form.ambiente.choices = [(ab.id,ab.descricao) for ab in AmbienteBanco.query.all()]
    if form.validate_on_submit():
        form = FormularioBancoTrabalho()
        banco_trabalho = BancoTrabalho(
                                        nome=form.nome.data,
                                        host_name=form.host_name.data,
                                        porta=form.porta.data,
                                        nome_database=form.nome_database.data, 
                                        usuario_database=form.usuario_database.data,
                                        senha_database=form.senha_database.data,
                                        id_ambiente_banco=form.ambiente.data,
                                        id_tipo_sgbd=form.tipo_sgbd.data   
                                    )
        db.session.add(banco_trabalho)
        db.session.commit()
        flash('Banco de Trabalho Cadastrado.','success')
        return redirect(url_for('bancos_trabalho.banco_trabalho_list'))
    elif request.method == 'GET':
        return render_template('banco_trabalho/new.html',titulo="Novo banco de trabalho",form=form)
    else:
        return render_template('banco_trabalho/new.html',titulo="Novo banco de trabalho",form=form)

@bancos_trabalho.route('/banco_trabalho/<int:id_banco_trabalho>')
@login_required
@require_admin()
def banco_trabalho(id_banco_trabalho):
    banco_trabalho = BancoTrabalho.query.get_or_404(id_banco_trabalho)
    form = FormularioBancoTrabalho()
    return render_template('banco_trabalho/show.html',banco_trabalho=banco_trabalho,form=form,title='Banco de Trabalho')

@bancos_trabalho.route('/banco_trabalho/<int:id_banco_trabalho>/atualizar',methods=['GET'])
@login_required
@require_admin()
def editar_banco_trabalho(id_banco_trabalho):
    banco_trabalho = BancoTrabalho.query.get_or_404(id_banco_trabalho)
    form = FormularioBancoTrabalho()
    form.nome.data = banco_trabalho.nome
    form.host_name.data = banco_trabalho.host_name
    form.porta.data = banco_trabalho.porta
    form.nome_database.data = banco_trabalho.nome_database
    form.usuario_database.data = banco_trabalho.usuario_database
    form.senha_database.data = banco_trabalho.senha_database
    form.tipo_sgbd.choices = [(ts.id,ts.descricao) for ts in TipoSGBD.query.all()]
    form.tipo_sgbd.data = banco_trabalho.id_tipo_sgbd
    form.ambiente.choices = [(ab.id,ab.descricao) for ab in AmbienteBanco.query.all()]
    form.ambiente.data = banco_trabalho.id_ambiente_banco

    return render_template('banco_trabalho/edit.html',
                            titulo='Atualizar Banco de Trabalho',
                            banco_trabalho=banco_trabalho,
                            form=form,
                            id_banco_trabalho=id_banco_trabalho)

@bancos_trabalho.route('/banco_trabalho/<int:id_banco_trabalho>/atualizar',methods=['POST'])
@login_required
@require_admin()
def atualizar_banco_trabalho(id_banco_trabalho):
    banco_trabalho = BancoTrabalho.query.get_or_404(id_banco_trabalho)
    form = FormularioBancoTrabalho()
    form.tipo_sgbd.choices = [(ts.id,ts.descricao) for ts in TipoSGBD.query.all()]
    form.ambiente.choices = [(ab.id,ab.descricao) for ab in AmbienteBanco.query.all()]
    if form.validate_on_submit():
        
        banco_trabalho.nome = form.nome.data
        banco_trabalho.host_name = form.host_name.data
        banco_trabalho.porta = form.porta.data
        banco_trabalho.nome_database = form.nome_database.data
        banco_trabalho.usuario_database = form.usuario_database.data
        banco_trabalho.senha_database= form.senha_database.data
        banco_trabalho.id_tipo_sgbd = form.tipo_sgbd.data
        banco_trabalho.id_ambiente_banco = form.ambiente.data
        
        db.session.commit()
        flash('Registro atualizado!','success')
        return redirect(url_for('bancos_trabalho.banco_trabalho_list'))
    else:
        return render_template('banco_trabalho/edit.html',
                            titulo='Atualizar Banco de Trabalho',
                            banco_trabalho=banco_trabalho,
                            form=form,
                            id_banco_trabalho=id_banco_trabalho)


@bancos_trabalho.route('/banco_trabalho/<int:id_banco_trabalho>/remover',methods=['GET','POST'])
@login_required
@require_admin()
def remover_banco_trabalho(id_banco_trabalho):
    print(id_banco_trabalho)
    banco_trabalho = BancoTrabalho.query.get_or_404(id_banco_trabalho)
    db.session.delete(banco_trabalho)
    db.session.commit()
    flash('O registro foi excluído.','success')
    return redirect(url_for('bancos_trabalho.banco_trabalho_list'))
    
