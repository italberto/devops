# -*- coding: utf-8 -*-
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField,HiddenField, IntegerField,SelectField,PasswordField
from wtforms.validators import DataRequired
from devops.models import BancoTrabalho
from devops.utils.validators import IPValidator

class FormularioBancoTrabalho(FlaskForm):
    nome = StringField('Nome',
                            validators=[DataRequired()])                            
    tipo_sgbd = SelectField('SGBD', 
                            validators=[DataRequired()])
    host_name = StringField('Hostname',
                            validators=[DataRequired(),IPValidator()])
    porta = IntegerField('Porta', validators=[DataRequired()])
    nome_database = StringField('Nome do Banco',validators=[DataRequired()])
    usuario_database = StringField('Usuario Banco',validators=[DataRequired()])
    senha_database = StringField('Senha Banco',validators=[DataRequired()])
    tipo_sgbd = SelectField('Tipo de SGBD', 
                            validators=[DataRequired()],coerce=int)
    ambiente = SelectField('Ambiente', 
                            validators=[DataRequired()],coerce=int)
    
    submit = SubmitField('Salvar')  
