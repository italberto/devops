# -*- coding: utf-8 -*-
from flask import render_template, request, Blueprint, flash, redirect,url_for
from devops.models import AmbienteBanco
from devops.ambiente_banco.forms import FormularioAmbienteBanco
from devops import db, app, bread
from flask_login import login_required
from devops.utils.check_roles import require_admin
from flask_breadcrumbs import register_breadcrumb

ambientes_banco = Blueprint('ambientes_banco',__name__,url_prefix='/ambientes_banco')

@ambientes_banco.route('/ambientes_banco')
@login_required
@require_admin()
@register_breadcrumb(ambientes_banco,'.','Ambiente Banco')
def ambiente_banco_list():
    page = request.args.get('page',1,type=int)
    total = len(AmbienteBanco.query.all())
    per_page = 10
    next = None
    previous = None
    if page > 1:
        previous = page - 1
    if page < total/per_page:
        next = page + 1 
    ambientes_banco = AmbienteBanco.query.paginate(page=page,per_page=per_page)
    return render_template('ambiente_banco/list.html',
                            title='Ambientes de Banco',
                            ambientes_banco=ambientes_banco,
                            next=next,
                            previous=previous,
                            total=total)

@ambientes_banco.route('/ambiente_banco/novo',methods=['GET','POST'])
@login_required
@require_admin()
@register_breadcrumb(app,'.','Ambiente Banco')
def ambiente_banco_novo():
    form = FormularioAmbienteBanco()
    if form.validate_on_submit():
        ambiente_banco = AmbienteBanco(descricao=form.descricao.data)
        db.session.add(ambiente_banco)
        db.session.commit()
        flash('Ambiente Cadastrado.','success')
        return redirect(url_for('ambientes_banco.ambiente_banco_list'))
    elif request.method == 'GET':
        return render_template('ambiente_banco/new.html',titulo="Novo Ambiente",form=form)
    else:
        return render_template('ambiente_banco/new.html',titulo="Novo Ambiente",form=form)

@ambientes_banco.route('/ambiente_banco/<int:id_ambiente_banco>')
@login_required
@require_admin()
@register_breadcrumb(app,'.','Ambiente Banco')
def ambiente_banco(id_ambiente_banco):
    ambiente_banco = AmbienteBanco.query.get_or_404(id_ambiente_banco)
    form = FormularioAmbienteBanco()
    form.id.data = ambiente_banco.id
    form.descricao.data = ambiente_banco.descricao
    return render_template('ambiente_banco/show.html',ambiente_banco=ambiente_banco,form=form,title='Ambiente de Banco')

@ambientes_banco.route('/ambiente_banco/<int:id_ambiente_banco>/atualizar',methods=['GET'])
@login_required
@require_admin()
@register_breadcrumb(app,'.','Ambiente Banco')
def editar_ambiente_banco(id_ambiente_banco):
    ambiente_banco = AmbienteBanco.query.get_or_404(id_ambiente_banco)
    form = FormularioAmbienteBanco()
    form.descricao.data = ambiente_banco.descricao
    return render_template('ambiente_banco/edit.html',
                            titulo='Atualizar Ambiente de Banco',
                            ambiente_banco=ambiente_banco,
                            form=form,
                            id_ambiente_banco=id_ambiente_banco)

@ambientes_banco.route('/ambiente_banco/<int:id_ambiente_banco>/atualizar',methods=['POST'])
@login_required
@require_admin()
@register_breadcrumb(app,'.','Ambiente Banco')
def atualizar_ambiente_banco(id_ambiente_banco):
    ambiente_banco = AmbienteBanco.query.get_or_404(id_ambiente_banco)
    form = FormularioAmbienteBanco()
    if form.validate_on_submit():
        ambiente_banco.descricao = form.descricao.data
        
        db.session.commit()
        flash('Registro atualizado!','success')
        return redirect(url_for('ambientes_banco.ambiente_banco_list'))
    else:
        return render_template('ambiente_banco/edit.html',
                            titulo='Atualizar Ambiente de Banco',
                            ambiente_banco=ambiente_banco,
                            form=form,
                            id_ambiente_banco=id_ambiente_banco)

@ambientes_banco.route('/ambiente_banco/<int:id_ambiente_banco>/remover',methods=['GET','POST'])
@login_required
@require_admin()
@register_breadcrumb(app,'.','Ambiente Banco')
def remover_ambiente_banco(id_ambiente_banco):
    print(id_ambiente_banco)
    ambiente_banco = AmbienteBanco.query.get_or_404(id_ambiente_banco)
    db.session.delete(ambiente_banco)
    db.session.commit()
    flash('O registro foi excluído.','success')
    return redirect(url_for('ambientes_banco.ambiente_banco_list'))
    
