# -*- coding: utf-8 -*-
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField,HiddenField
from wtforms.validators import DataRequired
from devops.models import TipoSolicitacao


class FormularioTipoSolicitacao(FlaskForm):
    id = HiddenField()
    descricao = StringField('Descrição',
                            validators=[DataRequired()])
    submit = SubmitField('Salvar') 
