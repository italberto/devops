# -*- coding: utf-8 -*-
from flask import render_template, request, Blueprint, flash, redirect,url_for
from devops.models import TipoSolicitacao
from devops.tipo_solicitacao.forms import FormularioTipoSolicitacao
from devops import db
from flask_login import login_required
from devops.utils.check_roles import require_admin

tipos_solicitacao = Blueprint('tipos_solicitacao',__name__)

@tipos_solicitacao.route('/tipos_solicitacao')
@login_required
@require_admin()
def tipo_solicitacao_list():
    page = request.args.get('page',1,type=int)
    total = len(TipoSolicitacao.query.all())
    per_page = 10
    next = None
    previous = None
    if page > 1:
        previous = page - 1
    if page < total/per_page:
        next = page + 1 
    tipos_solicitacao = TipoSolicitacao.query.paginate(page=page,per_page=per_page)
    return render_template('tipo_solicitacao/list.html',
                            title='Tipos de Solicitações',
                            tipos_solicitacao=tipos_solicitacao,
                            next=next,
                            previous=previous,
                            total=total)

@tipos_solicitacao.route('/tipo_solicitacao/novo',methods=['GET','POST'])
@login_required
@require_admin()
def tipo_solicitacao_novo():
    form = FormularioTipoSolicitacao()
    if form.validate_on_submit():
        tipo_solicitacao = TipoSolicitacao(descricao=form.descricao.data)
        db.session.add(tipo_solicitacao)
        db.session.commit()
        flash('Tipo de Solicitação cadastrada.','success')
        return redirect(url_for('tipos_solicitacao.tipo_solicitacao_list'))
    elif request.method == 'GET':
        return render_template('tipo_solicitacao/new.html',titulo="Novo Tipo de Solicitação",form=form)
    else:
        return render_template('tipo_solicitacao/new.html',titulo="Novo Tipo de Solicitação",form=form)

@tipos_solicitacao.route('/tipo_solicitacao/<int:id_tipo_solicitacao>')
@login_required
@require_admin()
def tipo_solicitacao(id_tipo_solicitacao):
    tipo_solicitacao = TipoSolicitacao.query.get_or_404(id_tipo_solicitacao)
    form = FormularioTipoSolicitacao()
    form.id.data = tipo_solicitacao.id
    form.descricao.data = tipo_solicitacao.descricao
    return render_template('tipo_solicitacao/show.html',tipo_solicitacao=tipo_solicitacao,form=form,title='Tipo de Solicitacao')

@tipos_solicitacao.route('/tipo_solicitacao/<int:id_tipo_solicitacao>/atualizar',methods=['GET'])
@login_required
@require_admin()
def editar_tipo_solicitacao(id_tipo_solicitacao):
    tipo_solicitacao = TipoSolicitacao.query.get_or_404(id_tipo_solicitacao)
    form = FormularioTipoSolicitacao()
    form.descricao.data = tipo_solicitacao.descricao
    return render_template('tipo_solicitacao/edit.html',
                            titulo='Atualizar Tipo de Solicitação',
                            tipo_solicitacao=tipo_solicitacao,
                            form=form,
                            id_tipo_solicitacao=id_tipo_solicitacao)


@tipos_solicitacao.route('/tipo_solicitacao/<int:id_tipo_solicitacao>/atualizar',methods=['POST'])
@login_required
@require_admin()
def atualizar_tipo_solicitacao(id_tipo_solicitacao):
    tipo_solicitacao = TipoSolicitacao.query.get_or_404(id_tipo_solicitacao)
    form = FormularioTipoSolicitacao()
    if form.validate_on_submit():
        tipo_solicitacao.descricao = form.descricao.data
        db.session.commit()
        flash('Registro atualizado!','success')
        return redirect(url_for('tipos_solicitacao.tipo_solicitacao_list'))
    else:
        return render_template('tipo_solicitacao/edit.html',
                        titulo='Atualizar Tipo de Solicitação',
                        tipo_solicitacao=tipo_solicitacao,
                        form=form,
                        id_tipo_solicitacao=id_tipo_solicitacao)



@tipos_solicitacao.route('/tipo_solicitacao/<int:id_tipo_solicitacao>/remover',methods=['GET','POST'])
@login_required
@require_admin()
def remover_tipo_solicitacao(id_tipo_solicitacao):
    print(id_tipo_solicitacao)
    
    tipo_solicitacao = TipoSolicitacao.query.get_or_404(id_tipo_solicitacao)
    print(tipo_solicitacao)
    db.session.delete(tipo_solicitacao)
    db.session.commit()
    flash('O registro foi excluído.','success')
    return redirect(url_for('tipos_solicitacao.tipo_solicitacao_list'))
    
