# -*- coding: utf-8 -*-
from flask import render_template, request, redirect, Blueprint
from devops.models import Solicitacao, SituacaoSolicitacao
from devops import app

dashboards = Blueprint('dashboards',__name__)

@dashboards.route('/dashboard/solicitacoes')
def solicitacoes():
    solicitacoes = Solicitacao.query.all()

    encerradas = Solicitacao.query.filter(Solicitacao.situacao.has(SituacaoSolicitacao.ativa == 0)).count()

    abertas = Solicitacao.query.filter(Solicitacao.situacao.has(SituacaoSolicitacao.ativa == 1)).count()
    
    total = len(solicitacoes)
    return render_template('dashboard/solicitacoes.html',
                             solicitacoes=solicitacoes,
                             total=total,
                             encerradas=encerradas,
                             abertas=abertas
                          )
