# -*- coding: utf-8 -*-
from flask_wtf import FlaskForm
from flask_login import current_user
from wtforms import StringField,SubmitField


class FormularioConsultaUsuarioRedMine(FlaskForm):
    nome = StringField('Nome')
    usuario = StringField('Usuario')                            
    submit = SubmitField('Salvar')  

class FormularioConsultaNumeroSolicitacao(FlaskForm):
    numero = StringField('Número da Solicitação')
    submit = SubmitField('Buscar')
