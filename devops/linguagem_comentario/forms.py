# -*- coding: utf-8 -*-
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField,HiddenField, IntegerField
from wtforms.validators import DataRequired
from devops.models import LinguagemSuportada

class FormularioLinguagemComentario(FlaskForm):
    id = IntegerField('ID')
    descricao = StringField('Descrição',
                            validators=[DataRequired()])
    submit = SubmitField('Salvar')  
