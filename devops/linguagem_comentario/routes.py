# -*- coding: utf-8 -*-
from flask import render_template, request, Blueprint, flash, redirect,url_for
from devops.models import LinguagemSuportada
from devops.linguagem_comentario.forms import FormularioLinguagemComentario
from devops import db
from flask_login import login_required
from devops.utils.check_roles import require_admin
from devops.utils.visual import calcular_paginas

linguagens_comentario = Blueprint('linguagens_comentario',__name__)

@linguagens_comentario.route('/linguagens')
@login_required
@require_admin()
def linguagem_comentario_list():
    page = request.args.get('page',1,type=int)
    per_page=10
    
    linguagens_comentario = LinguagemSuportada.query.paginate(page=page,per_page=per_page)
    return render_template('linguagem_comentario/list.html',
                            title='Tipos de SGBD',
                            linguagens=linguagens_comentario,
                            paginacao=calcular_paginas(linguagens_comentario))

@linguagens_comentario.route('/linguagem_comentario/novo',methods=['GET','POST'])
@login_required
@require_admin()
def linguagem_comentario_novo():
    form = FormularioLinguagemComentario()
    if form.validate_on_submit():
        linguagem_comentario = LinguagemSuportada(descricao=form.descricao.data)
        db.session.add(linguagem_comentario)
        db.session.commit()
        flash('linguagem_comentario Cadastrado.','success')
        return redirect(url_for('linguagens_comentario.linguagem_comentario_list'))
    elif request.method == 'GET':
        return render_template('linguagem_comentario/new.html',titulo="Novo linguagem_comentario",form=form)
    else:
        return render_template('linguagem_comentario/new.html',titulo="Novo linguagem_comentario",form=form)

@linguagens_comentario.route('/linguagem_comentario/<int:id_linguagem_comentario>')
@login_required
@require_admin()
def linguagem_comentario(id_linguagem_comentario):
    linguagem_comentario = LinguagemSuportada.query.get_or_404(id_linguagem_comentario)
    form = FormularioLinguagemComentario()
    form.id.data = linguagem_comentario.id
    form.descricao.data = linguagem_comentario.descricao

    return render_template('linguagem_comentario/show.html',linguagem_comentario=linguagem_comentario,form=form,title='linguagem_comentario')

@linguagens_comentario.route('/linguagem_comentario/<int:id_linguagem_comentario>/atualizar',methods=['GET'])
@login_required
@require_admin()
def editar_linguagem_comentario(id_linguagem_comentario):
    linguagem_comentario = LinguagemSuportada.query.get_or_404(id_linguagem_comentario)
    form = FormularioLinguagemComentario()
    form.descricao.data = linguagem_comentario.descricao
    
    return render_template('linguagem_comentario/edit.html',
                            titulo='Atualizar linguagem_comentario',
                            linguagem_comentario=linguagem_comentario,
                            form=form,
                            id_linguagem_comentario=id_linguagem_comentario)

@linguagens_comentario.route('/linguagem_comentario/<int:id_linguagem_comentario>/atualizar',methods=['POST'])
@login_required
@require_admin()
def atualizar_linguagem_comentario(id_linguagem_comentario):
    linguagem_comentario = LinguagemSuportada.query.get_or_404(id_linguagem_comentario)
    form = FormularioLinguagemComentario()
    if form.validate_on_submit():
        linguagem_comentario.descricao = form.descricao.data        
        db.session.commit()
        flash('Registro atualizado!','success')
        return redirect(url_for('linguagens_comentario.linguagem_comentario_list'))
    else:
        return render_template('linguagem_comentario/edit.html',
                            titulo='Atualizar linguagem_comentario',
                            linguagem_comentario=linguagem_comentario,
                            form=form,
                            id_linguagem_comentario=id_linguagem_comentario)


@linguagens_comentario.route('/linguagem_comentario/<int:id_linguagem_comentario>/remover',methods=['GET','POST'])
@login_required
@require_admin()
def remover_linguagem_comentario(id_linguagem_comentario):
    print(id_linguagem_comentario)
    
    linguagem_comentario = LinguagemSuportada.query.get_or_404(id_linguagem_comentario)
    print(linguagem_comentario)
    db.session.delete(linguagem_comentario)
    db.session.commit()
    flash('O registro foi excluído.','success')
    return redirect(url_for('linguagens_comentario.linguagem_comentario_list'))
