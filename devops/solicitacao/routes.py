# -*- coding: utf-8 -*-
from flask import render_template, request, Blueprint, flash, redirect, url_for, Blueprint, jsonify, make_response
from devops.solicitacao.forms import FormularioCadastroSolicitacao
from devops import db, app
from flask_login import login_required,current_user
from devops.models import Solicitacao, TipoSolicitacao, AmbienteBanco, BancoTrabalho, SituacaoSolicitacao, Usuario
from devops.utils.check_roles import require_admin, require_ativo
from devops.utils.visual import add_choices, calcular_paginas
from datetime import datetime
from devops.utils.sql import PostgresSqlExecutor
import time

solicitacoes = Blueprint('solicitacoes', __name__)

@solicitacoes.route('/solicitacoes')
@login_required
@require_admin()
def solicitacao_list():
    page = request.args.get('page', 1, type=int)
    total = len(Solicitacao.query.all())
    per_page = 10
    next = None
    previous = None
    if page > 1:
        previous = page - 1
    if page < total/per_page:
        next = page + 1
    solicitacoes = Solicitacao.query.paginate(page=page, per_page=per_page)
    return render_template('solicitacao/list.html',
                            title='Solicitações',
                            solicitacoes=solicitacoes,
                            next=next,
                            previous=previous,
                            total=total)


@solicitacoes.route('/solicitacao/novo', methods=['GET', 'POST'])
@login_required
@require_admin()
def solicitacao_novo():
    form = FormularioCadastroSolicitacao()

    add_choices(form.tipo_solicitacao, [
                (t.id, t.descricao) for t in TipoSolicitacao.query.all()])
    add_choices(form.ambiente, [(t.id, t.descricao)
                for t in AmbienteBanco.query.all()])
    add_choices(form.banco, [(t.id, t.nome)
                for t in BancoTrabalho.query.all()])
    add_choices(form.situacao, [(t.id, t.descricao)
                for t in SituacaoSolicitacao.query.all()])
    add_choices(form.solicitante, [(t.id, t.nome)
                for t in Usuario.query.all()])
    add_choices(form.responsavel, [(t.id, t.nome)
                for t in Usuario.query.all()])

    if form.validate_on_submit():
        data_solicitacao = datetime.utcnow
        solicitacao = Solicitacao(titulo=form.titulo.data,
                                  texto=form.texto.data,
                                  script=form.script.data,
                                  id_tipo_solicitacao=form.tipo_solicitacao.data,
                                  id_ambiente=form.ambiente.data,
                                  id_banco=form.banco.data,
                                  id_situacao=form.situacao.data,
                                  id_solicitante=form.solicitante.data,
                                  id_analista_responsavel=form.responsavel.data
                            )

        db.session.add(solicitacao)
        db.session.commit()
        flash('Solicitação cadastrado.', 'success')
        return redirect(url_for('solicitacoes.solicitacao_list'))
    elif request.method == 'GET':
        return render_template('solicitacao/new.html', titulo="Novo Solicitação", form=form)
    else:
        flash('Houve um erro ao cadastrar uma nova solicitação!')
        return render_template('solicitacao/new.html', titulo="Novo Solicitação", form=form)


@solicitacoes.route('/solicitacao/<int:id_solicitacao>')
@login_required
@require_admin()
def solicitacao(id_solicitacao):
    form = FormularioCadastroSolicitacao()
    solicitacao = Solicitacao.query.get_or_404(id_solicitacao)
    return render_template('solicitacao/show.html', solicitacao=solicitacao, form=form, title='Solicitação')


@solicitacoes.route('/solicitacao/<int:id_solicitacao>/atualizar', methods=['GET'])
@login_required
@require_admin()
def editar_solicitacao(id_solicitacao):
    solicitacao = Solicitacao.query.get_or_404(id_solicitacao)

    form = FormularioCadastroSolicitacao()

    add_choices(form.tipo_solicitacao, [
                (t.id, t.descricao) for t in TipoSolicitacao.query.all()])
    add_choices(form.ambiente, [(t.id, t.descricao)
                for t in AmbienteBanco.query.all()])
    add_choices(form.banco, [(t.id, t.nome)
                for t in BancoTrabalho.query.all()])
    add_choices(form.situacao, [(t.id, t.descricao)
                for t in SituacaoSolicitacao.query.all()])
    add_choices(form.solicitante, [(t.id, t.nome)
                for t in Usuario.query.all()])
    add_choices(form.responsavel, [(t.id, t.nome)
                for t in Usuario.query.all()])

    form.titulo.data = solicitacao.titulo
    form.texto.data = solicitacao.texto
    form.script.data = solicitacao.script
    form.tipo_solicitacao.data = solicitacao.id_tipo_solicitacao
    form.ambiente.data = solicitacao.id_ambiente
    form.banco.data = solicitacao.id_banco
    form.situacao.data = solicitacao.id_situacao
    form.solicitante.data = solicitacao.id_solicitante
    form.responsavel.data = solicitacao.id_analista_responsavel

    return render_template('solicitacao/edit.html',
                            titulo='Atualizar Solicitação',
                            solicitacao=solicitacao,
                            form=form,
                            id_solicitacao=id_solicitacao)


@solicitacoes.route('/solicitacao/<int:id_solicitacao>/atualizar', methods=['POST'])
@login_required
@require_admin()
def atualizar_solicitacao(id_solicitacao):
    solicitacao = Solicitacao.query.get_or_404(id_solicitacao)
    form = FormularioCadastroSolicitacao()

    add_choices(form.tipo_solicitacao, [
                (t.id, t.descricao) for t in TipoSolicitacao.query.all()])
    add_choices(form.ambiente, [(t.id, t.descricao)
                for t in AmbienteBanco.query.all()])
    add_choices(form.banco, [(t.id, t.nome)
                for t in BancoTrabalho.query.all()])
    add_choices(form.situacao, [(t.id, t.descricao)
                for t in SituacaoSolicitacao.query.all()])
    add_choices(form.solicitante, [(t.id, t.nome)
                for t in Usuario.query.all()])
    add_choices(form.responsavel, [(t.id, t.nome)
                for t in Usuario.query.all()])

    if form.validate_on_submit():

        solicitacao.titulo = form.titulo.data
        solicitacao.texto = form.texto.data
        solicitacao.script = form.script.data
        solicitacao.id_tipo_solicitacao = form.tipo_solicitacao.data
        solicitacao.id_ambiente = form.ambiente.data
        solicitacao.id_banco = form.banco.data
        solicitacao.id_situacao = form.situacao.data
        solicitacao.id_solicitante = form.solicitante.data
        solicitacao.id_analista_responsavel = form.responsavel.data

        db.session.commit()
        flash('Solicitação atualizado!', 'success')
        return redirect(url_for('solicitacoes.solicitacao_list'))
    else:
        return render_template('solicitacao/edit.html',
                            titulo='Atualizar Solicitação',
                            solicitacao=solicitacao,
                            form=form,
                            id_solicitacao=id_solicitacao)


@solicitacoes.route('/solicitacao/<int:id_solicitacao>/remover', methods=['GET', 'POST'])
@login_required
@require_admin()
def remover_solicitacao(id_solicitacao):
    solicitacao = Solicitacao.query.get_or_404(id_solicitacao)
    db.session.delete(solicitacao)
    db.session.commit()
    flash('Solicitação excluído.', 'success')
    return redirect(url_for('solicitacoes.solicitacao_list'))


@solicitacoes.route('/solicitacao/<int:id_solicitacao>/checar', methods=['POST'])
@login_required
@require_admin()
def checar_solicitacao(id_solicitacao):
    solicitacao = Solicitacao.query.get_or_404(id_solicitacao)
    resultado_execucao = []
    executor = PostgresSqlExecutor(solicitacao.banco.url_with_user())
    try:
        resultado_execucao = executor.check(solicitacao.script)
    except Exception as e:
        resultado_execucao = [False,str(e)]    

    response_body = {
        "result": resultado_execucao[0],
        "output": resultado_execucao[1]
    }

    if resultado_execucao[0]: #deu certo!
        solicitacao.checada = 1
        db.session.commit()
    else:
        solicitacao.checada = 0
        db.session.commit()

    response = make_response(jsonify(response_body), 200)

    return response


@solicitacoes.route('/solicitacao/<int:id_solicitacao>/executar', methods=['GET'])
@login_required
@require_admin()
def executar_solicitacao(id_solicitacao):
    solicitacao = Solicitacao.query.get_or_404(id_solicitacao)
    if solicitacao.checada:
        executor = PostgresSqlExecutor(solicitacao.banco.url_with_user())
        try:
            resultado_execucao = executor.execute(solicitacao.script)
            if resultado_execucao[0]: #Deu certo!
                situacao = SituacaoSolicitacao.query.filter_by(ativa=0,executada_sucesso=1).first()
                solicitacao.id_situacao = situacao.id
                solicitacao.data_finalizacao = datetime.now()
                db.session.commit()
            else:
                raise Exception('Houve um erro ao completar a operação de UPDATE')
        except e:
            print(e)
            flash('Houve um erro na execução da solicitação.')
            return redirect(url_for('solicitacoes.solicitacao',id_solicitacao=solicitacao.id))        
        else:
            flash('Solicitação executada com sucesso!')
        
        return redirect(url_for('solicitacoes.solicitacao',id_solicitacao=solicitacao.id))
    else:
        flash('A solicitação não foi checada ainda.')
        return redirect(url_for('solicitacoes.solicitacao',id_solicitacao=solicitacao.id))

@solicitacoes.route('/solicitacao/<int:id_solicitacao>/cancelar', methods=['GET','POST'])
@login_required
@require_admin()
def cancelar_solicitacao(id_solicitacao):
    solicitacao = Solicitacao.query.get_or_404(id_solicitacao)
    situacao = SituacaoSolicitacao.query.filter_by(ativa=0,executada_sucesso=0).first()
    solicitacao.id_situacao = situacao.id
    solicitacao.data_finalizacao = datetime.now()
    if not solicitacao.responsavel: 
        solicitacao.id_responsavel = current_user.id
    db.session.commit()
    flash('A solicitação foi cancelada.')
    return redirect(url_for('solicitacoes.solicitacao',id_solicitacao=solicitacao.id))

@solicitacoes.route('/solicitacao/<int:id_solicitacao>/reabrir', methods=['GET','POST'])
@login_required
@require_admin()
def reabrir_solicitacao(id_solicitacao):
    solicitacao = Solicitacao.query.get_or_404(id_solicitacao)
    situacao = SituacaoSolicitacao.query.filter_by(ativa=1,padrao_nova=1).first()
    solicitacao.id_situacao = situacao.id
    solicitacao.data_finalizacao = None
    solicitacao.id_responsavel = current_user.id
    db.session.commit()
    flash('A solicitação foi reaberta.')
    return redirect(url_for('solicitacoes.solicitacao',id_solicitacao=solicitacao.id))

@solicitacoes.route('/solicitacao/<int:id_solicitacao>/pegar', methods=['GET','POST'])
@login_required
@require_admin()
def pegar_solicitacao(id_solicitacao):
    solicitacao = Solicitacao.query.get_or_404(id_solicitacao)
    solicitacao.id_analista_responsavel = current_user.id
    db.session.commit()
    flash('Agora você é o responsável por essa solicitação.')
    return redirect(url_for('solicitacoes.solicitacao',id_solicitacao=solicitacao.id))    

@solicitacoes.route('/solicitacao/<int:id_usuario>/filtro', methods=['GET'])
@login_required
@require_admin()
def solicitacao_filter_usuario(id_usuario):

    per_page = 10
    page = request.args.get('page',1,type=int)
    solicitacoes = Solicitacao.query.filter_by(id_solicitante=id_usuario).paginate(page=page,per_page=per_page)
    total = len(solicitacoes.items)
   
    next = None
    previous = None
    if page > 1:
        previous = page - 1
    if page < total/per_page:
        next = page + 1 
    
    return render_template('solicitacao/list.html',
                            title='Solicitações',
                            solicitacoes=solicitacoes,
                            next=next,
                            previous=previous,
                            total=total)



@solicitacoes.route('/solicitacao/minhas/abertas')
@login_required
@require_ativo()
def minhas_solicitacoes_abertas():
    

    per_page = 10
    page = request.args.get('page',1,type=int)


    solicitacoes = Solicitacao.query.filter(Solicitacao.id_solicitante == current_user.id,
                                            Solicitacao.situacao.has(SituacaoSolicitacao.ativa == 1)).paginate(page=page,per_page=per_page) 

   
    return render_template('solicitacao/solicitante/listar_minhas_solicitacoes.html',
                            title='Minhas solicitações abertas',
                            solicitacoes=solicitacoes,
                            paginacao=calcular_paginas(solicitacoes,page=page))


@solicitacoes.route('/solicitacao/minhas/encerradas')
@login_required
@require_ativo()
def minhas_solicitacoes_encerradas():
    per_page = 10
    page = request.args.get('page',1,type=int)


    solicitacoes = Solicitacao.query.filter(Solicitacao.id_solicitante == current_user.id,
                                            Solicitacao.situacao.has(SituacaoSolicitacao.ativa == 0)).paginate(page=page,per_page=per_page)

    return render_template('solicitacao/solicitante/listar_minhas_solicitacoes.html',
                            title='Minhas solicitações abertas',
                            solicitacoes=solicitacoes,
                            paginacao=calcular_paginas(solicitacoes,page=page))

@solicitacoes.route('/solicitar/nova', methods=['GET', 'POST'])
@login_required
@require_ativo()
def solicitar_nova():
    form = FormularioCadastroSolicitacao()

    add_choices(form.tipo_solicitacao, [(t.id, t.descricao) for t in TipoSolicitacao.query.all()])
    add_choices(form.ambiente, [(t.id, t.descricao) for t in AmbienteBanco.query.all()])
    add_choices(form.banco, [(t.id, t.nome) for t in BancoTrabalho.query.all()])
    add_choices(form.situacao, [(t.id, t.descricao) for t in SituacaoSolicitacao.query.all()])
    add_choices(form.solicitante, [(t.id, t.nome) for t in Usuario.query.all()])
    add_choices(form.responsavel, [(t.id, t.nome) for t in Usuario.query.all()])

    situacao = SituacaoSolicitacao.query.filter_by(padrao_nova=1).first()

    form.situacao.data = situacao.id
    form.solicitante.data = current_user.id
    form.responsavel.data = 0

    if form.validate_on_submit():
        data_solicitacao = datetime.utcnow
        
        solicitacao = Solicitacao(titulo=form.titulo.data,
                                  texto=form.texto.data,
                                  script=form.script.data,
                                  id_tipo_solicitacao=form.tipo_solicitacao.data,
                                  id_ambiente=form.ambiente.data,
                                  id_banco=form.banco.data,
                                  id_situacao=form.situacao.data,
                                  id_solicitante=form.solicitante.data,
                                  id_analista_responsavel=None
                            )

        db.session.add(solicitacao)
        db.session.commit()
        flash('Solicitação cadastrada.', 'success')
        return redirect(url_for('solicitacoes.minhas_solicitacoes_abertas'))
    elif request.method == 'GET':
        return render_template('solicitacao/solicitante/solicitar.html',titulo='Nova Solicitação',form=form)
    else:
        flash('Houve um erro ao cadastrar uma nova solicitação!')
        return render_template('solicitacao/solicitante/solicitar.html',titulo='Nova Solicitação',form=form)
        
    

@solicitacoes.route('/solicitacao/<int:id_solicitacao>/acompanhar')
@login_required
@require_ativo()
def acompanhar(id_solicitacao):
    form = FormularioCadastroSolicitacao()
    solicitacao = Solicitacao.query.get_or_404(id_solicitacao)
    if solicitacao.solicitante.id == current_user.id:
        return render_template('solicitacao/solicitante/acompanhar.html', solicitacao=solicitacao, form=form, title='Solicitação')    
    return render_template('index')



    
