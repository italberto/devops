# -*- coding: utf-8 -*-
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField,IntegerField,SelectField,PasswordField,BooleanField,TextAreaField,DateTimeField
from wtforms.validators import DataRequired, Length, Email
from devops.models import Usuario, TipoSolicitacao, AmbienteBanco,BancoTrabalho, SituacaoSolicitacao
from flask_login import current_user

class FormularioCadastroSolicitacao(FlaskForm):
    id = IntegerField('ID')
    titulo = StringField('Título', 
                            validators=[DataRequired()]
                          )
    texto =  TextAreaField('Texto',validators=[DataRequired()],render_kw={'class': 'form-control', 'rows': 3})
    script = TextAreaField('Script',validators=[DataRequired()],render_kw={'class': 'form-control', 'rows': 5})

    data_solicitacao = DateTimeField('Data de Solicitação')

    tipo_solicitacao = SelectField('Tipo de Solicitação',coerce=int)
    ambiente = SelectField('Ambiente',coerce=int)
    banco = SelectField('Banco',coerce=int)
    situacao = SelectField('Situação',coerce=int)
    solicitante = SelectField('Solicitante',coerce=int)
    responsavel = SelectField('Responsável',coerce=int)

    ativo = BooleanField('Ativo')
    submit = SubmitField('Salvar')
    submit_solicitar = SubmitField('Solicitar')


  
