# -*- coding: utf-8 -*-
from sqlalchemy import create_engine
import sqlalchemy
from sqlalchemy.exc import OperationalError
import logging
import io


class SqlExecutor(object):
    def __init__(self,url):
        self.url = url        

    def check(self,command):
        raise NotImplementedError
        

    def execute(self,command):
        raise NotImplementedError

class PostgresSqlExecutor(SqlExecutor):
    def __init__(self,url):
        SqlExecutor.__init__(self,url)
        self.engine = create_engine(url,echo=False,logging_name='sqlalchemy-pg',pool_recycle=3600)
    
    def check(self,script):
        
        connection = self.engine.connect()

        ### Create the logger
        logger = logging.getLogger('sqlalchemy-pg')
        logger.setLevel(logging.INFO)

        ### Setup the console handler with a StringIO object
        log_capture_string = io.StringIO()
        ch = logging.StreamHandler(log_capture_string)
        ch.setLevel(logging.INFO)

        ### Optionally add a formatter
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        ### Add the console handler to the logger
        logger.addHandler(ch)
        transaction = connection.begin()
        ok = False
        try:
            result = connection.execute(sqlalchemy.text(script))
            ok = True
        except Exception as e:
            logger.error(e)
            print(e)
            raise OperationalError('Houve um erro ao conectar no banco.')
        finally:
            connection.close()
            transaction.rollback()
            connection.close()  

        

        ### Pull the contents back into a string and close the stream
        log_contents = log_capture_string.getvalue()
        log_capture_string.close()

        if ok:
            retorno = 'O script não apresenta erros.\n{}\n{}'.format('-'*30,log_contents)
            return[ok,retorno]
        return [ok,log_contents]
    
    def execute(self,script):
        
        connection = self.engine.connect()

        transaction = connection.begin()
        ok = False
        try:
            result = connection.execute(sqlalchemy.text(script))
            ok = True
            transaction.commit()
        except Exception as e:
            transaction.rollback()
        finally:
            connection.close()
        

        return [ok,'Executado']



