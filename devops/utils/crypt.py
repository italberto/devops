# -*- coding: utf-8 -*-
from devops import bcrypt
import hashlib 
from devops import app
import secrets

BCRYPT = 1
MD5 = 2


def generate_hash(valor,metodo=BCRYPT):
    if metodo == MD5:
        return hashlib.md5(valor.encode()).hexdigest()
    elif metodo == BCRYPT:
        return bcrypt.generate_password_hash(valor).decode('utf-8')
    return None

def check_password(senha_atual,senha_fornecida):
    metodo = app.config['METODO_ENCRIPTACAO']
    if metodo == BCRYPT:
        if bcrypt.check_password_hash(senha_atual, senha_fornecida):
            return True
    elif metodo == MD5:
        hashed = generate_hash(senha_fornecida,metodo=MD5)
        if senha_atual == hashed:
            return True
    return False

