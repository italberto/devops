# -*- coding: utf-8 -*-
from devops import app

def app_name():
    return app.config['APP_NAME']

def add_choices(field,choices):
    default = [(0,'')]
    field.choices = default + choices
    
def calcular_paginas(model,page=1,per_page=app.config['PER_PAGE']):
    total = len(model.items)
    next = None
    previous = None
    if page > 1:
        previous = page - 1
    if page < total/per_page:
        next = page + 1

    return {'total':total,'previous':previous,'next':next}    
