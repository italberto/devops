from colorama import Fore

def aviso(msg):
    print(Fore.YELLOW + msg + Fore.RESET)
    

def erro(msg):
    print(Fore.RED + msg + Fore.RESET)
    

def sucesso(msg):
    print(Fore.GREEN + msg + Fore.RESET)
    