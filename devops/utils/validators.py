# -*- coding: utf-8 -*-
import re
from wtforms.validators import ValidationError

class IPValidator(object):
    def __init__(self, message=None):
        if message:
            self.message = message
        else:
            self.message = "IP Inválido"
        

    def __call__(self, form, field):
        ip = str(field.data)
        if not self.validate_ip(ip):
            raise ValidationError(self.message)    
    
    def validate_ip(self,ip): 
        regex = '''^(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.( 
                25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.( 
                25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.( 
                25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)'''
        
        valid = False
        if re.search(regex, ip):
            valid =  True 
        if valid:
            partes = ip.split('.')
            for p in partes:
                if int(p)>255:
                    return False
        return valid
