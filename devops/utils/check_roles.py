# -*- coding: utf-8 -*-
from devops.models import Usuario,TipoUsuario
from functools import wraps
from flask import redirect,flash,url_for
from flask_login import current_user

def require_admin(*roles):
   def wrapper(f):
      @wraps(f)
      def wrapped(*args, **kwargs):
         if not is_admin():
            flash('Perfil não autorizado para a funcionalidade solicitada.','error')
            return redirect(url_for('index'))
         return f(*args, **kwargs)
      return wrapped
   return wrapper
 
def require_ativo(*roles):
   def wrapper(f):
      @wraps(f)
      def wrapped(*args, **kwargs):
         if not is_ativo():
            flash('Seu usuário ainda não foi ativado.','error')
            return redirect(url_for('index'))
         return f(*args, **kwargs)
      return wrapped
   return wrapper

def is_admin():
    if current_user.is_admin():
        return True
    return False

def is_ativo():
   if current_user.is_ativo():
      return True
   return False