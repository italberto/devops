# -*- coding: utf-8 -*-
from flask import render_template, flash, redirect, url_for, request
from devops import app, db, bcrypt
from flask_login import login_user, current_user, logout_user, login_required
from flask_admin.contrib.sqla import ModelView
from devops.forms import FormularioConsultaNumeroSolicitacao
from devops.usuario.forms import FormularioCadastroUsuario
from devops.models import Solicitacao

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/entrar')
def entrar():
    return render_template('index.html')

@app.route("/perfil",methods=['GET','POST'])
@login_required
def perfil():
    form = FormularioCadastroUsuario()
    if form.validate_on_submit():
        current_user.login = form.login_field.data
        current_user.email = form.email.data
        db.session.commit()
        flash('Seu perfil foi atualizado!','success')
        return redirect(url_for('perfil'))
    elif request.method == 'GET':
        form.login_field.data = current_user.login
        form.email.data = current_user.email
    image_file = url_for('static',filename='images/profile/2.jpg')
    return render_template('perfil/perfil.html',title='Perfil', image_file=image_file,form=form)

@app.errorhandler(404)
def page_not_found(error):
    return render_template('erros/404.html')

@app.errorhandler(500)
def page_not_found(error):
    return render_template('erros/500.html')    

@app.route('/consulta/',methods=['POST'])
@login_required
def consulta_solicitacao():
    form = FormularioConsultaNumeroSolicitacao()
    solicitacao = Solicitacao.query.get_or_404(form.numero.data)
    if solicitacao.solicitante.id == current_user.id:
        return render_template('solicitacao/solicitante/acompanhar.html', solicitacao=solicitacao, form=form, title='Solicitação')    
    return render_template('index')

@app.route('/help')
def help():
    return render_template('about/help.html')

@app.route('/roadmap')
def roadmap():
    return render_template('about/roadmap.html')    

@app.route('/todo')
def todo():
    return render_template('about/todo.html')
