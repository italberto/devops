# -*- coding: utf-8 -*-
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField,HiddenField, IntegerField
from wtforms.validators import DataRequired
from devops.models import Setor

class FormularioSetor(FlaskForm):
    id = IntegerField('ID')
    nome = StringField('Nome',
                            validators=[DataRequired()])                            
    numero_sala = StringField('Número da Sala',
                            validators=[DataRequired()])                            
    submit = SubmitField('Salvar')  
