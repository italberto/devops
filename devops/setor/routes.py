# -*- coding: utf-8 -*-
from flask import render_template, request, Blueprint, flash, redirect,url_for
from devops.models import Setor
from devops.setor.forms import FormularioSetor
from devops import db
from flask_login import login_required
from devops.utils.check_roles import require_admin

setores = Blueprint('setores',__name__)


@setores.route('/setores')
@login_required
@require_admin()
def setor_list():
    page = request.args.get('page',1,type=int)
    total = len(Setor.query.all())
    per_page = 10
    next = None
    previous = None
    if page > 1:
        previous = page - 1
    if page < total/per_page:
        next = page + 1 
    setores = Setor.query.paginate(page=page,per_page=per_page)
    return render_template('setor/list.html',
                            title='Tipos de SGBD',
                            setores=setores,
                            next=next,
                            previous=previous,
                            total=total)

@setores.route('/setor/novo',methods=['GET','POST'])
@login_required
@require_admin()
def setor_novo():
    form = FormularioSetor()
    if form.validate_on_submit():
        setor = Setor(nome=form.nome.data,numero_sala=form.numero_sala.data)
        db.session.add(setor)
        db.session.commit()
        flash('Setor Cadastrado.','success')
        return redirect(url_for('setores.setor_list'))
    elif request.method == 'GET':
        return render_template('setor/new.html',titulo="Novo Setor",form=form)
    else:
        return render_template('setor/new.html',titulo="Novo Setor",form=form)

@setores.route('/setor/<int:id_setor>')
@login_required
@require_admin()
def setor(id_setor):
    setor = Setor.query.get_or_404(id_setor)
    form = FormularioSetor()
    form.id.data = setor.id
    form.nome.data = setor.nome
    form.numero_sala.data = setor.numero_sala
    return render_template('setor/show.html',setor=setor,form=form,title='Setor')   

@setores.route('/setor/<int:id_setor>/atualizar',methods=['GET'])
@login_required
@require_admin()
def editar_setor(id_setor):
    setor = Setor.query.get_or_404(id_setor)
    form = FormularioSetor()
    form.nome.data = setor.nome
    form.numero_sala.data = setor.numero_sala
    return render_template('setor/edit.html',
                            titulo='Atualizar Setor',
                            setor=setor,
                            form=form,
                            id_setor=id_setor)

@setores.route('/setor/<int:id_setor>/atualizar',methods=['POST'])
@login_required
@require_admin()
def atualizar_setor(id_setor):
    setor = Setor.query.get_or_404(id_setor)
    form = FormularioSetor()
    if form.validate_on_submit():
        setor.nome = form.nome.data
        setor.numero_sala = form.numero_sala.data
        
        db.session.commit()
        flash('Registro atualizado!','success')
        return redirect(url_for('setores.setor_list'))
    else:
        return render_template('setor/edit.html',
                            titulo='Atualizar Setor',
                            setor=setor,
                            form=form,
                            id_setor=id_setor)


@setores.route('/setor/<int:id_setor>/remover',methods=['GET','POST'])
@login_required
@require_admin()
def remover_setor(id_setor):
    print(id_setor)
    
    setor = Setor.query.get_or_404(id_setor)
    print(setor)
    db.session.delete(setor)
    db.session.commit()
    flash('O registro foi excluído.','success')
    return redirect(url_for('setores.setor_list'))
