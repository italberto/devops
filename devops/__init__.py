from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_admin.contrib.sqla import ModelView
from flask_breadcrumbs import Breadcrumbs  
import os
import secrets

DEFAULT_DB_LOCATION = ''
DEFAULT_DB_NAME = 'devops.sqlite'

DB_PATH = os.getenv('DB_PATH')

app = Flask(__name__)
app.config['APP_NAME'] = '/dev/ops'
#app.config['SECRET_KEY'] = 'asjflsajfljsaflkjwlkjldsf'
app.config['SECRET_KEY'] = secrets.token_hex(32)

print(app.config['SECRET_KEY'])

#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + DB_PATH
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres://postgres:postgres@localhost:5432/devops'
#app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('DB_PATH')

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['FOO_PASSWD'] = 'foopassword'
app.config['PER_PAGE'] = 10

db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
bread = Breadcrumbs(app=app)
login_manager = LoginManager(app)
login_manager.login_view = 'login' #nome da funcao de login. usada pelo login_required
login_manager.login_message_category = 'info'

import devops.utils.crypt as crypt

app.config['METODO_ENCRIPTACAO'] = crypt.MD5


@login_manager.user_loader
def load_usuario(id_usuario):
    return Usuario.query.get(int(id_usuario))
    
from devops import routes
from devops.tipo_solicitacao.routes import tipos_solicitacao
from devops.grupo_banco.routes import grupos_banco
from devops.acesso.routes import acesso
from devops.ambiente_banco.routes import ambientes_banco
from devops.linguagem_comentario.routes import linguagens_comentario
from devops.setor.routes import setores
from devops.tipo_sgbd.routes import tipos_sgbd
from devops.banco_trabalho.routes import bancos_trabalho
from devops.usuario.routes import usuarios
from devops.dashboard.routes import dashboards
from devops.situacao_solicitacao.routes import situacoes_solicitacao
from devops.solicitacao.routes import solicitacoes

app.register_blueprint(solicitacoes)
app.register_blueprint(tipos_solicitacao)
app.register_blueprint(acesso)
app.register_blueprint(grupos_banco)
app.register_blueprint(ambientes_banco)
app.register_blueprint(linguagens_comentario)
app.register_blueprint(setores)
app.register_blueprint(tipos_sgbd)
app.register_blueprint(bancos_trabalho)
app.register_blueprint(usuarios)
app.register_blueprint(dashboards)
app.register_blueprint(situacoes_solicitacao)

from devops.models import Usuario, TipoUsuario
from devops.models import AmbienteBanco,BancoTrabalho
from devops.models import GrupoBanco,Setor,TipoSGBD
from devops.models import TipoSolicitacao,SituacaoSolicitacao

from .db_cli import init_db
app.cli.add_command(init_db)

