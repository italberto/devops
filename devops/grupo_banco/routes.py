# -*- coding: utf-8 -*-
from flask import render_template, request, Blueprint, flash, redirect,url_for
from devops.models import GrupoBanco
from devops.grupo_banco.forms import FormularioGrupoBanco
from devops import db
from flask_login import login_required
from devops.utils.check_roles import require_admin

grupos_banco = Blueprint('grupos_banco',__name__)

@grupos_banco.route('/grupos_banco')
@login_required
@require_admin()
def grupo_banco_list():
    page = request.args.get('page',1,type=int)
    total = len(GrupoBanco.query.all())
    per_page = 10
    next = None
    previous = None
    if page > 1:
        previous = page - 1
    if page < total/per_page:
        next = page + 1 
    grupos_banco = GrupoBanco.query.paginate(page=page,per_page=per_page)
    return render_template('grupo_banco/list.html',
                            title='Grupos de Bancos',
                            grupos_banco=grupos_banco,
                            next=next,
                            previous=previous,
                            total=total)

@grupos_banco.route('/grupo_banco/novo',methods=['GET','POST'])
@login_required
@require_admin()
def grupo_banco_novo():
    form = FormularioGrupoBanco()
    if form.validate_on_submit():
        grupo_banco = GrupoBanco(descricao=form.descricao.data)
        db.session.add(grupo_banco)
        db.session.commit()
        flash('Grupo Cadastrado.','success')
        return redirect(url_for('grupos_banco.grupo_banco_list'))
    elif request.method == 'GET':
        return render_template('grupo_banco/new.html',titulo="Novo Grupo de Banco",form=form)
    else:
        return render_template('grupo_banco/new.html',titulo="Novo Grupo de Banco",form=form)

@grupos_banco.route('/grupo_banco/<int:id_grupo_banco>')
@login_required
@require_admin()
def grupo_banco(id_grupo_banco):
    grupo_banco = GrupoBanco.query.get_or_404(id_grupo_banco)
    form = FormularioGrupoBanco()
    form.id.data = grupo_banco.id
    form.descricao.data = grupo_banco.descricao
    return render_template('grupo_banco/show.html',grupo_banco=grupo_banco,form=form,title='Grupo de Banco')

@grupos_banco.route('/grupo_banco/<int:id_grupo_banco>/atualizar',methods=['GET'])
@login_required
@require_admin()
def editar_grupo_banco(id_grupo_banco):
    grupo_banco = GrupoBanco.query.get_or_404(id_grupo_banco)
    form = FormularioGrupoBanco()
    form.descricao.data = grupo_banco.descricao
    return render_template('grupo_banco/edit.html',
                            titulo='Atualizar Grupo de Banco',
                            grupo_banco=grupo_banco,
                            form=form,
                            id_grupo_banco=id_grupo_banco)
        

@grupos_banco.route('/grupo_banco/<int:id_grupo_banco>/atualizar',methods=['POST'])
@login_required
@require_admin()
def atualizar_grupo_banco(id_grupo_banco):
    grupo_banco = GrupoBanco.query.get_or_404(id_grupo_banco)
    form = FormularioGrupoBanco()
    if form.validate_on_submit():
        grupo_banco.descricao = form.descricao.data
        
        db.session.commit()
        flash('Registro atualizado!','success')
        return redirect(url_for('grupos_banco.grupo_banco_list'))
    else:
        return render_template('grupo_banco/edit.html',
                            titulo='Atualizar Grupo de Banco',
                            grupo_banco=grupo_banco,
                            form=form,
                            id_grupo_banco=id_grupo_banco)


@grupos_banco.route('/grupo_banco/<int:id_grupo_banco>/remover',methods=['GET','POST'])
@login_required
@require_admin()
def remover_grupo_banco(id_grupo_banco):
    print(id_grupo_banco)
    grupo_banco = GrupoBanco.query.get_or_404(id_grupo_banco)
    db.session.delete(grupo_banco)
    db.session.commit()
    flash('O registro foi excluído.','success')
    return redirect(url_for('grupos_banco.grupo_banco_list'))
    
