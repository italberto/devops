import sqlite3
from sqlite3 import Error
import os
from os import path
import click
from flask.cli import with_appcontext
from . import db, app, DB_PATH
from .models import Usuario,TipoUsuario,Setor
from .utils.crypt import generate_hash
from .utils.console import erro, aviso, sucesso

def create_db(db_file):
    """ create a database connection to a SQLite database """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        aviso(db_file)
    except Error as e:
        erro(e)
    finally:
        if conn:
            conn.close()

def create_tables():
	db.create_all()
	db.session.commit()
 
def add_admin_user(usuario,email,senha,nome):
	setor_default = Setor(nome='Default',numero_sala='1')
	usuario = Usuario(login=usuario,email=email,senha=generate_hash(senha,metodo=app.config['METODO_ENCRIPTACAO']),nome=nome,
					  ativo=1,id_tipo_usuario=1,id_setor=1)	

	db.session.add(usuario)
	db.session.commit()

@click.command(name="initdb",help="Inicializa  o banco de dados.")
@with_appcontext
def init_db():
	db_path = os.getenv('DB_PATH')
	if db_path:
		if not path.exists(db_path):
			create_db(db_path)
			create_tables()
			add_admin_user('admin','admin@admin.com','admin','Administrador')
			sucesso('The database was initalized successfully.')
		else:
			aviso('The file already exists. Nothing was changed.')
	else:    	
		aviso('You must define the DB_PATH environment variable pointing to sqlite3 database. Sorry, try again!')
